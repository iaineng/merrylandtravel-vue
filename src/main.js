import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import '@/styles/index.scss'
import { defaultSettings } from '@/settings'
import { setCookie } from '@/utils/cookie'

import Vue from 'vue'
import App from './App.vue'

Vue.prototype.$settings = defaultSettings

import { registerLangFunction } from '@/lang'

registerLangFunction(Vue)

Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = true

new Vue({
  render: h => h(App)
}).$mount('#app')

window.debugSetLang = function(lang) {
  setCookie('lang', lang, 1)
}
