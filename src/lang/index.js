import zhCn from './zh_CN.json'
import enUs from './en_US.json'

export function registerLangFunction(vue) {
  vue.prototype.$t = function(internationalCode) {
    if (this.$settings.lang === 'zh_CN') {
      return zhCn[internationalCode]
    } else if (this.$settings.lang === 'en_US') {
      return enUs[internationalCode]
    }
    return null
  }
}
