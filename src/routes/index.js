import Home from '@/views/home/index'
import TourDetails from '@/views/tourDetails/index'
import Tours from '@/views/tours/index'
import AboutUs from '@/views/aboutUs/index'
import Article from '@/views/article/index'
import Articles from '@/views/articles/index'
import GroupTours from '@/views/groupTours/index'
import ReleasePackage from '@/views/releasePackge/index'
import Knowledge from '@/views/knowledge/index'

require('js-url')

const routes = {
  '': Home,
  '/tour-detail': TourDetails,
  '/tours': Tours,
  '/about-us': AboutUs,
  '/article': Article,
  '/articles': Articles,
  '/group-tours': GroupTours,
  '/release-package': ReleasePackage,
  '/knowledge': Knowledge
}

export const RouteView = routes[window.url('path', window.location.pathname)]
