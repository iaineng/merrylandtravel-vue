import request from '@/utils/request'

export function getReleasePack(params) {
  return request({
    url: '/api/getReleasePackage.php',
    method: 'get',
    params
  })
}

export function getGroupTours(params) {
  return request({
    url: '/api/getGroupTours.php',
    method: 'get',
    params
  })
}

export function getArticles(params) {
  return request({
    url: '/api/getArticles.php',
    method: 'get',
    params
  })
}

export function getExchangeRate(params) {
  return request({
    url: '/api/getExchangeRate.php',
    method: 'get',
    params
  })
}

export function getNavbar(params) {
  return request({
    url: '/api/getNavbar.php',
    method: 'get',
    params
  })
}

export function getAboutUs(params) {
  return request({
    url: '/api/getAboutUs.php',
    method: 'get',
    params
  })
}

export function getBanner(params) {
  return request({
    url: '/api/getBanner.php',
    method: 'get',
    params
  })
}

export function getSpecialPromotion(params) {
  return request({
    url: '/api/getSpecialPromotion.php',
    method: 'get',
    params
  })
}

export function getChinaTourGuide(params) {
  return request({
    url: '/api/getChinaTourGuide.php',
    method: 'get',
    params
  })
}

export function getWorldTourGuide(params) {
  return request({
    url: '/api/getWorldTourGuide.php',
    method: 'get',
    params
  })
}

export function getLeastArticle(params) {
  return request({
    url: '/api/getLeastArticle.php',
    method: 'get',
    params
  })
}

export function getCountryCards(params) {
  return request({
    url: '/api/getCountryCards.php',
    method: 'get',
    params
  })
}
