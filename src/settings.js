import { setCookie, getCookie } from '@/utils/cookie'

if (getCookie('lang') === '') {
  setCookie('lang', 'zh_CN', 1)
}

const defaultSettings = {
  lang: getCookie('lang')
}

export { defaultSettings }
